<?php

// composer require symfony/routing
// composer require symfony/http-kernel
// composer require symfony/templating

// https://www.sitepoint.com/build-php-framework-symfony-components/

$loader = require 'vendor/autoload.php';

$loader->register();

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Templating\PhpEngine;
use Symfony\Component\Templating\TemplateNameParser;
use Symfony\Component\Templating\Loader\FilesystemLoader;

require 'core.php';
$request = Request::createFromGlobals();

$filesystemLoader = new FilesystemLoader(__DIR__.'/views/%name%');

$templating = new PhpEngine(new TemplateNameParser(), $filesystemLoader);




// Our Framework is now handling itself the request
$app = new Framework\Core();

$app->map('/', function () {
    return new Response('This is the home page');
});

$app->map('/hello/{name}', function ($name) use ($templating, $request) {
		return new Response($templating->render('hello.php', array('firstname' => $name)));
	});


$app->map('/about', function () {
	
    return new Response('This is the about page');
});



$response = $app->handle($request);

$response->send();