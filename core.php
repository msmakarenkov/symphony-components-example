<?php 
namespace Framework;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface as HttpKernelInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;


class Core implements HttpKernelInterface
{
    /** @var RouteCollection */
    protected $routes;
    public function __construct()
	{
		$this->routes = new RouteCollection();
	}


    public function handle(Request $request, $type = HttpKernelInterface::MASTER_REQUEST, $catch = true)
    {
        $path = $request->getPathInfo();
       

        $context = new RequestContext();
		$context->fromRequest($request);

		$matcher = new UrlMatcher($this->routes, $context);


        // Does this URL match a route?
        if (array_key_exists($path, $this->routes)) {
            // execute the callback
            $controller = $this->routes[$path];
            $response = $controller();
        } else {
            // no route matched, this is a not found.
            $response = new Response('Not found!', Response::HTTP_NOT_FOUND);
        }

        try {
			$attributes = $matcher->match($request->getPathInfo());
			$controller = $attributes['controller'];
			unset($attributes['controller']);
			$response = call_user_func_array($controller, $attributes);
		} catch (ResourceNotFoundException $e) {
			$response = new Response('Not found!', Response::HTTP_NOT_FOUND);
		}

        return $response;
    }

    // Associates an URL with a callback function
    public function map($path, $controller) {
				$this->routes->add($path, new Route(
					$path,
					array('controller' => $controller)
				));
			}

}